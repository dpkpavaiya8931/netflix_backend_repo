const request = require('supertest');
const app = require('../app');
const assert = require('chai').assert;

describe('Test suite for movie microservice', () => {
  it('should create movie and return movie', (done) => {
    const movie = {
        id: '1',
        name: 'padmaavat',
        path: 'xyz',
        poster: 'img',
        type: 'bollywood',
        genre: 'drama'
    };
    request(app)
      .post('/api/v1/movie')
      .send({
        query: `mutation{
            addMovie(input:{
                id: "${movie.id}"
                name:"${movie.name}"
                path: "${movie.path}"
                poster: "${movie.poster}"
                type: "${movie.type}"
                genre: "${movie.genre}
            })
            {id, name}
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should have body in response');
        assert.property(
          res.body.data,
          'addMovie',
          'Should have property addMovie'
        );
        assert.equal(
          res.body.data.addMovie.id,
          movie.id,
          'Should return movie'
        );
        done();
      });
  });
  it('should delete movie with given name', (done) => {
    const Movie = {
        id: '1',
        name: 'padmaavat',
        path: 'xyz',
        poster: 'img',
        type: 'bollywood',
        genre: 'drama'
    };
    request(app)
      .post('/api/v1/movie')
      .send({
        query: `mutation{deleteMovie(
            movieName:"${Movie.name}"
            ){ 
               id
               path
               poster
               type
               genre
            }
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should have body in response');
        assert.property(
          res.body.data,
          'deleteMovie',
          'Should have property deleteMovie'
        );
        assert.equal(
            res.body.data.deleteMovie.id,
            Movie.id,
            'Should delete movie id'
          );
        assert.equal(
          res.body.data.deleteMovie.path,
          Movie.path,
          'Should delete movie path'
        );
        assert.equal(
          res.body.data.deleteMovie.poster,
          Movie.poster,
          'Should delete movie poster'
        );
        assert.equal(
          res.body.data.deleteMovie.type,
          Movie.type,
          'Should delete movie status'
        );
        assert.equal(
            res.body.data.deleteMovie.genre,
            Movie.genre,
            'Should delete movie genre'
          );
        done();
      });
  });
  it('should return movie with given name', (done) => {
    const newMovie = {
        id: '1',
        name: 'padmaavat',
        path: 'xyz',
        poster: 'img',
        type: 'bollywood',
        genre: 'drama'
    };
    request(app)
      .post('/api/v1/movie')
      .send({
        query: `{getMovie(
            movieName:"${newMovie.name}"
            ){ id
               path
               poster
               type
               genre
            }
        }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should have body in response');
        assert.property(
          res.body.data,
          'getMovie',
          'Should have property getMovie'
        );
        assert.equal(
            res.body.data.getMovie.id,
            newMovie.id,
            'Should return movie id'
          );
        assert.equal(
          res.body.data.getMovie.path,
          newMovie.path,
          'Should return movie path'
        );
        assert.equal(
          res.body.data.getMovie.poster,
          newMovie.poster,
          'Should return movie poster'
        );
        assert.equal(
          res.body.data.getMovie.type,
          newMovie.type,
          'Should return movie status'
        );
        assert.equal(
            res.body.data.getMovie.genre,
            newMovie.genre,
            'Should return movie genre'
          );
        done();
      });
  });
  
});
