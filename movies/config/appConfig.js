const serverConfig = {
  port: 3010,
  hostname: '127.0.0.1'
};

const dbConfig = {
  url: 'mongodb://localhost/movies'
};

module.exports = {
  serverConfig,
  dbConfig
};
