const { buildSchema } = require('graphql');

module.exports = buildSchema(`
    type Movie{
        id: String!
        name: String!
        path: String!
        poster: String!
        type: String!
        genre: String!
    }

    input movieInput{
        id: String!
        name: String!
        path: String!
        poster: String!
        type: String!
        genre: String!
    }

    type RootQuery{
        getMovies(token:String!): [Movie!]!
        getMovie(token:String!,movieName: String!): Movie!
    }

    type RootMutation{
        addMovie(token:String!,input: movieInput!):Movie!
        deleteMovie(token:String!,movieName:String!):Movie!
    }

    schema{
        query: RootQuery
        mutation: RootMutation
    }
`);
