const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const movieSchema = new Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  path: {
    type: String,
    required: true
  },
  poster: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  genre: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('movie', movieSchema);
