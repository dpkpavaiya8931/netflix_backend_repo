const movieDAO = require("./movie.dao");

const resolver = {
  ...movieDAO,
};

module.exports = resolver;
