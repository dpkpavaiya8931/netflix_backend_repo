const router = require("express").Router();
const { graphqlHTTP } = require("express-graphql");
const schema = require("./movie.schema");
const resolver = require("./movie.resolver");

router.use(
  "/movies",
  graphqlHTTP({
    schema,
    rootValue: resolver,
    graphiql: true,
  })
);

module.exports = router;
