const Movie = require('./movie.entity');
const User = require('./user.entity');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  addMovie: async ({token},{ movieInput }) => {
    try {
      const token = jwt.verify(token,
        "12345hello",
        {
            expiresIn: '15hr'
        }
      );
      if(token.admin="yes"){
        const movie = new Movie({
          movieid: movieInput.movieid,
          name: movieInput.name,
          path: movieInput.path,
          poster: movieInput.poster,
          type: movieInput.type,
          genre: movieInput.genre,
          userid: movieInput.userid,
          releaseDate: movieInput.releaseDate,
          updatedOn: movieInput.updatedOn
        });
        const existingMovie = await Movie.findOne({
          movieid: movie.movieid,
          name: movie.name
        });  
        if (existingMovie) {
          throw new Error('Movie already exists');
        }  
        const result = await movie.save();  
        return result;
      }else{
        throw new Error("User is not a Admin");
      }
    }catch (error) {
      throw new Error(error);
    }
  },
  deleteMovie: async ({token},{ movieName }) => {
    try {
      const validUser = jwt.verify(token,
        "12345hello",
        {
            expiresIn: '15hr'
        }
      );
      if(validUser.admin="yes"){
        
        const existingMovie = await Movie.findOne({name:movieName});  
        if (!existingMovie) {
          throw new Error("movie Does not Exist");
        }  
      }else{
       const result = await Movie.deleteOne({name:movieName});
       return result;
      }
    }catch (error) {
      throw new Error(error);
    }
  },
  getMovies: async ({ token }) => {
    try {
      const token = jwt.verify(token,
        "12345hello",
        {
            expiresIn: '15hr'
        }
      );
      const user = await User.findOne({email:token.email});
      if(!user){
        throw new Error("Not a Valid User")
      }else{
        const movies = await Movie.find();
        return movies;
      }
    } catch (error) {
      throw new Error(error);
    }
  },

  getMovie: async ({ token },{movieName}) => {
    try {
      const valid = jwt.verify(token,
        "12345hello",
        {
            expiresIn: '15hr'
        }
      );
      const user = await User.findOne({email:valid.email});
      if(!user){
        throw new Error("Not a Valid User")
      }else{
        if(user.paid === "yes"){
          const movie = await Movie.find({name:movieName});
          return movie;
        }        
      }
    } catch (error) {
      throw new Error(error);
    }
  }
};
