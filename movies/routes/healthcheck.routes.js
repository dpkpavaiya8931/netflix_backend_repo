// const express = require('express');
// const healthCheck = require('healthchecks-api');
// const router = express.Router({});
// const app = express();

// router.get('/',async(req,res,next)=>{
//     // const healthcheck = {
//     //     uptime: process.uptime(),
//     //     message: 'OK',
//     //     timestamp: Date.now()
//     // };
//     // try{
//     //     res.send(healthcheck);
//     // }catch(e){
//     //     healthcheck.message = e;
//     //     res.status(500).send();
//     // }

//     let response = await healthCheck(app,
//         {
//             adapter: 'express',
//             service: {
//                 config: {
//                    name: 'demo-app',
//                 //    description: 'Nice demo application :)',
//                 //    statsLinks: [ 'https://my-stats/demo-app' ],
//                 //    logsLinks: [ 'https://my-logs/demo-app/info', 'https://my-logs/demo-app/debug' ],
//                    checks: [
//                        {
//                            name: 'mongo',
//                            url: 'mongodb://mongo/admincreds',
//                            type: 'internal',
//                            interval: 3000,
//                            check: 'mongo',
//                        },
//                        {
//                            name: 'service-1',
//                            url: 'http://localhost:8020/api/v1/movies',
//                            interval: 1000,
//                            check: 'http',
//                        }
//                     ]
//                 },
//             },
//         })
//     res.send(response)

// })

// module.exports = router;