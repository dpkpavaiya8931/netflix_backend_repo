const { get } = require("mongoose")

describe('healthcheck',() =>{
    it('return 200 if server is healthy', async()=>{
        const res = await get(`/healthcheck`,null)
            .expect(200);
        expect(res.body.uptime).toBeGreaterThan(0);
    });
});