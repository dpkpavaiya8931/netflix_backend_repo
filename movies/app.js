const express = require("express");
const app = express();
const appService = require("./app.service");
const fs = require("fs");
const lighthouse = require("lighthouse");
const chromeLauncher = require('chrome-launcher');

appService.connect();
appService.setMiddleware(app);
appService.apiSetUp(app);

(async () => {
    const chrome = await chromeLauncher.launch({chromeFlags: ['--headless']});
    const options = {logLevel: 'info', output:'html', Categories: ['Performance'], port: chrome.port};
    options.strategy = "desktop";  // "mobile"
    const runnerResult = await lighthouse('https://www.realtor.com', options);

    const reportHtml = runnerResult.report;
    fs.writeFileSync('lhreport.html', reportHtml);

    console.log("Report os done for", runnerResult.lhr.finalUrl);
    console.log("Performance score was", runnerResult.lhr.categories.performance.score * 100);
    await chrome.kill();
})();

// app.use('/health',require('./routes/healthcheck.routes'));

module.exports = app;
