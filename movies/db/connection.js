const mongoose = require("mongoose");
const { dbConfig } = require("../config");

const createConnection = () => {
  mongoose.connect(dbConfig.url);
};

const getConnection = () => {
  return mongoose.connection;
};

module.exports = {
  createConnection,
  getConnection,
};
