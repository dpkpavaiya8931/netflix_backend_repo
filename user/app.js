const express = require('express');
const app = express();
const appService = require('./app.service');
const cors = require('cors');


app.use(cors());
appService.connect();
appService.setAppMiddleware(app);
appService.apiSetUp(app);

module.exports = app;
