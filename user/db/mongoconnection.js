const mongoose = require('mongoose');
const { dbConfig } = require('../config').appConfig;

const createMongoConnection = () => {
    //console.log(dbConfig.mongoUrl);
    
    mongoose.connect(dbConfig.mongoUrl);
}
const getMongoConnection = () => {
    return mongoose.connection;
}

module.exports = {
    createMongoConnection,
    getMongoConnection
}
