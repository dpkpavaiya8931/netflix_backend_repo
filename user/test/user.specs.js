const request = require('supertest');
const assert = require('chai').assert;
const app = require('../app');

describe('Test suite for user microservice', (done) => {
  it('Should register user and return username', (done) => {
    let user = {
        email: 'xyz',
        username: 'testUser12',
        password: '12345',
        mobile: '94794643',
        paid: 'yes',
        admin: 'abc'
    };
    request(app)
      .post('/api/v1/user')
      .send({
        query: `mutation{register(userInput:{
           email:"${user.email}"
            username:"${user.username}"
            password:"${user.password}"
            mobile: "${user.mobile}"
            paid: "${user.paid}"
            admin: "${user.admin}"
        })
        { username }
    }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should not be null');
        assert.property(
          res.body.data,
          'register',
          'Should have property register'
        );
        assert.property(
          res.body.data.register,
          'username',
          'res.body should have property username'
        );
        assert.equal(
          res.body.data.register.username,
          user.username.toLocaleLowerCase(),
          'Name of registered user should be returned'
        );
      });
    done();
  });
  it('Should login user and return logged in username', (done) => {
    let User = {
        email: 'xyz',
        password: '12345'
        
    };
    request(app)
      .get('/api/v1/user')
      .send({
        query: `{login(userInput:{
            email:"${User.email}"
            password:"${User.password}"
            
         }){username}}`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should not be null');
        assert.property(
          res.body.data,
          'login',
          'Should have property register'
        );
        
      });
    done();
  });
  it('should return movie array for particular email', (done) => {
    const fav = {
      email: 'xyz'
    };
    request(app)
      .post('/api/v1/user')
      .send({
        query: `{getFav(
                email:"${fav.email}"
                ){
                    movieName
                }
            }`
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        assert.isNotNull(res.body, 'Should have body in response');
        assert.property(
          res.body.data,
          'getFav',
          'Should have property getFav'
        );
        assert.isArray(res.body.data.getFav, 'Should array of movies');
        assert.equal(
          res.body.data.getFav[0].movieName,
          'Call manager',
          'Should return movie name'
        );
        done();
      });
});
});