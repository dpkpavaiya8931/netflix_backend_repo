const router = require('express').Router();
const { graphqlHTTP } = require('express-graphql');
const graphqlSchema = require('./user/user.schema');
const graphqlResolver = require('./user');


router.use(
    '/user',
    graphqlHTTP({
        schema: graphqlSchema,
        rootValue: graphqlResolver,
        graphiql:true
    })
);

module.exports = router
