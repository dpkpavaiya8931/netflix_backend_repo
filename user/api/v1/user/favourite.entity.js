const mongoos = require('mongoose');

const Schema = mongoos.Schema;

const favouriteSchema = new Schema({
    id:{
        type:String,
        required:true,
        unique:true
    },
    title:{
        type:String,
        required:true
    },
    poster_path:{
        type:String,
        required:true
    },
    vote_average:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    }
})

module.exports = mongoos.model('favourite',favouriteSchema);