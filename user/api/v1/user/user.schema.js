
const { buildSchema } = require('graphql');

module.exports  = buildSchema(`
    type AuthData{
        id:ID!
        email:String!
        admin:String!
        token : String!
        paid:String!
        tokenExpiration : Int!
        username:String!
        mobile:String!
    }
    type Sucess{
        result : Int!
    }
    type fav{
        id:Int!
    }
    type user{
        id:ID!
        email:String!
        username : String!
        password : String!
        mobile:String!
        paid:String!
        admin:String!
    }
    input registerUser{
        email : String!
        username : String!
        password : String!
        mobile : String!
        paid : String!
        admin : String!
    }
    input loginUser{
        email : String!
        password : String!
    }
    type favMovieList{
        id:Int!
        title:String!,
        poster_path:String!
        vote_average:String!
    }
    input favMovie{
        id:Int!
        title:String!,
        poster_path:String!
        vote_average:String!
        email:String!
    }

    type RootQuery{
        login(loginUser:loginUser!):AuthData!
    }

    type RootMutation{
        register(userInput: registerUser):user    
        addFav(favMovie:favMovie!):Sucess 
        remFav(id:Int!,email:String!):Sucess
        getFav(id:Int!,email:String!):Sucess
        updatePayment(email:String!,payment:String!):Sucess
        getFavList(email:String!): [favMovieList!]!
        resetPassword(email:String!,oldPassword:String!,newPassword:String!):Sucess
    }

    schema{
        query : RootQuery
        mutation : RootMutation
    }

`);
