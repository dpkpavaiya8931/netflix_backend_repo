const mongoos = require('mongoose');

const Schema = mongoos.Schema;

const userSchema = new Schema({
    id:{
        type:String,
        required:true,
        unique:true
    },
    email:{
        type:String,
        required:true
    },
    username : {
        type:String,
        required:true,
        lowercase:true
    },
    password :{
        type:String,
        required:true
    },
    mobile:{
        type:String,
        required:true
    },
    paid:{
        type:String,
        required:true
    },
    admin:{
        type:String,
        required:true
    }
})

module.exports = mongoos.model('user',userSchema);
