const authResolver = require('./user.auth');

const rootResolver = {
    ...authResolver
}

module.exports = rootResolver;
