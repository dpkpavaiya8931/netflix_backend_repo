const User = require('./user.entity');
const Fav = require('./favourite.entity');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { exception } = require('console');
const { db } = require('./user.entity');


module.exports = {

//------------------------------- User Registration functionality--------------------------------------------
    register : async args => {
        console.log(args.userInput);
        try{
            const existingUser = await User.findOne({email : args.userInput.email})
            if(existingUser){
                throw new Error('User exists already ');
            }
            const hashPassword = await bcrypt.hash(args.userInput.password,12);
            const user = new User({
                id:Math.random()*1000,
                email: args.userInput.email,
                username: args.userInput.username,
                password: hashPassword,
                mobile:args.userInput.mobile,
                paid:args.userInput.paid,
                admin: args.userInput.admin
            })
            const result = await user.save();
            return {...result._doc,email:result.email,admin:result.admin};           
        }
        catch(error){
            console.log(error);            
        }
    },

//------------------------------- User Login functionality--------------------------------------------
    login : async (args)=>{
        const user = await User.findOne({email:args.loginUser.email});
        if(!user){
            throw new Error('Username is not valid');
        }
        const isEqual = await bcrypt.compare(args.loginUser.password,user.password);
        if(!isEqual){
            throw new Error('Invalid password');
        }
        const token = jwt.sign({
            email : user.email,
            username : user.username,
            admin :user.admin,
            paid:user.paid
        },
        "12345hello",
        {
            expiresIn: '15hr'
        }
        );
        return {mobile:user.mobile,paid:user.paid,username:user.username,token:token,tokenExpiration:15,email:user.email};
    },

//------------------------------- Add to Favourite functionality--------------------------------------------
    addFav: async (args)=>{
        try{
            const exist = await Fav.findOne({id:args.id})
            if(exist){
                throw new Error("movie Already Present");
            }else{
                const addFav = new Fav({
                    id:args.favMovie.id,
                    title:args.favMovie.title,
                    poster_path:args.favMovie.poster_path,
                    vote_average:args.favMovie.vote_average,
                    email:args.favMovie.email
                })
                const result = await addFav.save();
                if(result.id == args.favMovie.id){
                    return {result:1};
                }else{
                    return {result:0};
                }
            }
        }catch(error){
            console.log(error);
        }
    },

//------------------------------- Rmove from Favourite functionality--------------------------------------------
    remFav: async (args)=>{
        try{
            // const exist = await Fav.findOne({movieName:args.movieName,email:args.email});
            const result = await Fav.deleteOne({id:args.id,email:args.email});  
            console.log(result.ok); 
            if(result.ok == 1){
                return {result:1};
            }else{
                return {result:0};
            }

        }catch(error){
            console.log(error);
        }
    },

    
//------------------------------- Get Single Favourite Movies functionality--------------------------------------------
    getFav: async (args)=>{
        const result = await Fav.find({id:args.id,email:args.email});
        if(result[0].id == args.id){
            return {result:1};
        }else{
            return {result:0};
        }        
    },


//------------------------------- Payment functionality--------------------------------------------
    updatePayment: async (args)=>{
        try{
            const user = await User.findOne({email:args.email});
            const updation = await User.update({email:user.email},{paid:args.payment});
            if(updation.ok == 1){
                return {result : 1};
            }else{
                return {result : 0};
            }
        }catch(error){
            throw new Error(error);
        }        
    },


//-----------------------------------Get All Favourite Movie List---------------------------------------------
    getFavList : async (args)=>{
        try{
            const result = await Fav.find({email:args.email});
            return result;
        }catch(error){
            console.log(error);
        }
    },


//-----------------------------------Reset Password Functionality---------------------------------------------
    resetPassword : async (args)=>{
        try{
            const user = await User.findOne({email:args.email});
            if(user){
                const isEqual = await bcrypt.compare(args.oldPassword,user.password);
                if(!isEqual){
                    return "Current password is Incorrect";
                }else{
                    const hashPassword = await bcrypt.hash(args.newPassword,12);
                    const result = await User.update({email:user.email},{password:hashPassword});
                    if(result.ok == 1){
                        return { result : 1 };
                    }else{
                        return { result : 0 }
                    }
                }
            }else{
                return "User not found";
            }
        }catch(error){
            console.log(error);
        }
    }
}
