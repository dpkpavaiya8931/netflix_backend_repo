const bodyparser = require('body-parser');
const api = require('./api/v1');

const db = require('./db');


const connect = () => {
    db.createMongoConnection();
    dbConnection = db.getMongoConnection();
}

const setAppMiddleware = (app) => {
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({extended:false}));
}

const apiSetUp = (app) => {
    app.use('/api/v1',api);
}

module.exports  = {
    connect,
    setAppMiddleware,
    apiSetUp
}